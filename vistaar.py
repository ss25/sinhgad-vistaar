import streamlit as st
import pandas as pd
import numpy as np
import altair as alt

# Custom Styling
st.markdown(
    '''
    <style>
        .block-container{
            padding-top: 0rem;
            max-width: 80%;
        }
        .css-1bh6xo1.e1fqkh3o2 {
            padding-top: 1rem;
            max-width: 100%;
        }
        footer {
            display: none !important;
        }
    </style>
    ''',
    unsafe_allow_html=True
)

###########
st.sidebar.title("सिंहगड - संकल्प सूची - 2022-23")
nagar = st.sidebar.selectbox(
    'नगर', ("भाग एकत्रित", "विवेकानंद", "विठ्ठलवाडी", "वडगाव", "नऱ्हे", "धायरी", "नांदेड")
)
st.sidebar.markdown('---')
col1, col2, col3 = st.columns(3)
# with col3:
#     nagar = st.selectbox(
#         'नगर', ("भाग एकत्रित", "विवेकानंद", "विठ्ठलवाडी", "वडगाव", "नऱ्हे", "धायरी", "नांदेड")
#     )

st.markdown('#')
st.subheader(nagar)
st.markdown('---')
chart_data = pd.read_excel('assets/Bhaag-sankalp.xlsx',  engine='openpyxl')
if nagar == "भाग एकत्रित":
    filtered = chart_data
else:
    filtered = chart_data[chart_data['Nagar'].str.contains(nagar)]

c = alt.Chart(filtered).mark_circle().encode(
     x='Vasti', y='Shakha Prakaar', size='Up Prakaar')
st.altair_chart(c, use_container_width=True)

bar = alt.Chart(filtered).mark_bar().encode(
    y='Vasti',
    x='count(Prakaar)',
    color='Prakaar')

st.altair_chart(bar, use_container_width=True)

bar1 = alt.Chart(filtered).mark_bar().encode(
    y='Vasti',
    x='count(Shakha Prakaar)',
    color='Shakha Prakaar')
st.altair_chart(bar1, use_container_width=True)

# st.bar_chart(chart_data)
